package view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.LayoutManager;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import de.javasoft.swing.ToolBar;
import view.route.RoutePanel;
import view.store.StorePanel;
import view.travel.TravelPanel;

public class App extends JFrame {

	public static App instatnce;
	public static final String TRAVEL = "travel";
	public static final String ROUTE = "rout";
	public static final String STORE = "Store";
	private JPanel contentPane;
	private JPanel mainPanel;
	private CardLayout layout;
	private TravelPanel panel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App frame = new App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public App() {
		instatnce = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		AminToolbar toolBar = new AminToolbar();
		contentPane.add(toolBar,BorderLayout.NORTH);
		
		mainPanel = new JPanel();
		layout = new CardLayout(0,0);
		mainPanel.setLayout(layout);
		
		contentPane.add(mainPanel, BorderLayout.CENTER);
		
		panel = new TravelPanel();
		mainPanel.add(panel,TRAVEL);
		
		JPanel routePanel = new RoutePanel();
		mainPanel.add(routePanel,ROUTE);
		
		JPanel storePanel = new StorePanel();
		mainPanel.add(storePanel,STORE);
		
		
	}
	
	@Override
	public LayoutManager getLayout() {
		return super.getLayout();
	}
	
	public void showView(String viewName){
		layout.show(mainPanel, viewName);
	}

}
