package view.general;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import de.javasoft.swing.ToolBar;

public class TableToolbar extends ToolBar {
	private static final long serialVersionUID = 1L;
	private JButton addButton;
	@Override
	public Border getBorder() {
		return new EmptyBorder(0,0,0,0);
	}
	@Override
	public Insets getInsets() {
		return new Insets(0, -3, 0, -3);
	}
	
	public TableToolbar() {
		super();
		setOrientation(SwingConstants.HORIZONTAL);
		setFloatable(false);
		setOpaque(false);
		setBorder(new EmptyBorder(3, 3, 3, 3));
		
		addButton = new JButton();
		
		addButton.setToolTipText("Add");
		addButton.setIcon(resources.icons.Icons.ADD);
		addButton.setMaximumSize(new Dimension(30, 30));
		add(addButton);
	}
	
	public void setAddActionListner(ActionListener addActionListner) {
		addButton.addActionListener(addActionListner);
	}
}
