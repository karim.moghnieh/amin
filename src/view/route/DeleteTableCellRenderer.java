package view.route;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import resources.icons.Icons;

public class DeleteTableCellRenderer extends DefaultTableCellRenderer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel label;
	
	public DeleteTableCellRenderer() {
		label=new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);	
		label.setIcon(Icons.DELETE);
		label.setOpaque(true);
	}
	
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
			
			if(isSelected)
			{
				label.setBackground(table.getSelectionBackground());
				label.setForeground(table.getSelectionForeground());
			}
			else
			{
				label.setBackground(table.getBackground());
				label.setForeground(table.getForeground());
				
			}
						
		if(table.isEnabled())
			label.setEnabled(true);
		else
			label.setEnabled(false);
		return label;
	}

}
