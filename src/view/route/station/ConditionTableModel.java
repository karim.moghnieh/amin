package view.route.station;

import javax.swing.table.AbstractTableModel;

import facade.StationFacade;
import model.route.Station;

public class ConditionTableModel extends AbstractTableModel{
	public static final int deleteButtonIndex = 1;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StationFacade facade = new StationFacade();
	private Station station = new Station();
	@Override
	public int getRowCount() {
		return station.getConditions().size();
	}

	@Override
	public int getColumnCount() {
		return 2;
	}
	
	@Override
	public String getColumnName(int column) {
		if(column == deleteButtonIndex){
			return "Delete";
		}
		return "Condition";
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if(columnIndex == deleteButtonIndex){
			return "";
		}
		return station.getConditions().get(rowIndex);
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		station.getConditions().set(rowIndex, aValue.toString());
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 0;
	}
	
	public void setStation(Station station) {
		this.station = station;
		fireTableDataChanged();
	}
	
	public void addcondition(){
		station.getConditions().add("");
		int index = station.getConditions().size() -1;
		fireTableRowsInserted(index, index);
		facade.save(station);
	}
	
	public void removeCondition(int index){
		station.getConditions().remove(index);
		fireTableRowsDeleted(index, index);
		facade.save(station);
	}

}
