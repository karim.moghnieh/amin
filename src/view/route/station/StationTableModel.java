package view.route.station;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import facade.StationFacade;
import model.route.Station;

public class StationTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private List<Station> alLStations = new ArrayList<>();
	
	private static final Class<?>[] COLUMS_CLASS = {Integer.class, String.class, String.class,String.class};
	private static final String[] COLUMS_NAME = {"ID", "Name", "Description", "Delete"};
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return COLUMS_CLASS[columnIndex];
	}
	
	@Override
	public String getColumnName(int column) {
		return COLUMS_NAME[column];
	}
	
	@Override
	public int getColumnCount() {
		return COLUMS_NAME.length;
	}
	
	@Override
	public int getRowCount() {
		return alLStations.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Station s = alLStations.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return s.getId();
		case 1:
			return s.getName();
		case 2:
			return s.getDescription();

		default:
			break;
		}
		return null;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Station s = alLStations.get(rowIndex);
		switch (columnIndex) {
		case 1:
			s.setName(aValue.toString());
			break;
		case 2:
			s.setDescription(aValue.toString());
			break;
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(columnIndex == 1 || columnIndex ==2){
			return true;
		}
		return false;
	}
	
	public void addNewStation(){
		Station s = new Station();
		s = new StationFacade().save(s);
		alLStations.add(0, s);
		fireTableRowsInserted(0, 0);
	}
	
	public void removeStation(int rwoIndex){
		Station remove = alLStations.remove(rwoIndex);
		new StationFacade().remove(remove);
		fireTableRowsDeleted(rwoIndex, rwoIndex);
	}
	
	public List<Station> getAlLStations() {
		return alLStations;
	}
	
	public void setAlLStations(List<Station> alLStations) {
		this.alLStations = alLStations;
	}

}
