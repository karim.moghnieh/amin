package view.route.station;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import de.javasoft.swing.JYTable;
import de.javasoft.swing.JYTableHeader;
import de.javasoft.swing.jytable.renderer.CellLayoutHint;
import view.App;
import view.route.DeleteTableCellRenderer;

public class ConditionTable extends JYTable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ConditionTableModel model = new ConditionTableModel();
	
	public ConditionTable() {
		super(model);
	    JYTableHeader header =  (JYTableHeader)getTableHeader();
	    CellLayoutHint hint = header.getCellLayoutHint();
	    //center header text
	    header.setCellLayoutHint(new CellLayoutHint(hint.sortMarkerPosition, SwingConstants.CENTER, hint.verticalAlignment));
	    
	    ConditionTableMouseListner listner = new ConditionTableMouseListner();
	    addMouseListener(listner);
	    
	    getColumn(ConditionTableModel.deleteButtonIndex).setCellRenderer(new DeleteTableCellRenderer());
	    
	    setTerminateEditOnFocusLost(true);
	}
	
	@Override
	public ConditionTableModel getModel() {
		return model;
	}
	
	private class ConditionTableMouseListner extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			if(e.getSource() == null || !(e.getSource() instanceof JTable)){
				return;
			}
			JTable table = (JTable) e.getSource();

			int row = table.rowAtPoint(e.getPoint());
			if(row <0) return;
			int modelRow = table.convertRowIndexToModel(row);
			ConditionTableModel tableModel = (ConditionTableModel) table.getModel();
			
			if(table.columnAtPoint(e.getPoint()) == ConditionTableModel.deleteButtonIndex){
				int answer = JOptionPane.showOptionDialog(App.instatnce, "Delete row", "are you sure ?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] { "Yes",  "No"}, JOptionPane.NO_OPTION);
				if (answer != JOptionPane.YES_OPTION)
					return;
				tableModel.removeCondition(modelRow);
			}
		}
	}

	
	

}
