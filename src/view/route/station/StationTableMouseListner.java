package view.route.station;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import facade.StationFacade;
import model.route.Station;
import view.App;
import view.route.RoutePanel;

public class StationTableMouseListner extends MouseAdapter {

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getSource() == null || !(e.getSource() instanceof JTable)){
			return;
		}
		JTable table = (JTable) e.getSource();

		int row = table.rowAtPoint(e.getPoint());
		if(row <0) return;
		int modelRow = table.convertRowIndexToModel(row);
		StationTableModel tableModel = (StationTableModel) table.getModel();
		
		if(table.columnAtPoint(e.getPoint()) == 3){
			int answer = JOptionPane.showOptionDialog(App.instatnce, "Delete row", "are you sure ?",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] { "Yes",  "No"}, JOptionPane.NO_OPTION);
			if (answer != JOptionPane.YES_OPTION)
				return;
			tableModel.removeStation(modelRow);
		}else{
			Station s = tableModel.getAlLStations().get(modelRow);
			ConditionTable conditionTable = RoutePanel.getConditionTableView().getConditionTable();
			if(conditionTable.isEditing()){
				conditionTable.getCellEditor().cancelCellEditing();
			}
			conditionTable.getModel().setStation(s);
		}
	}
}
