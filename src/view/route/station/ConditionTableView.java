package view.route.station;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import de.javasoft.swing.JYTableScrollPane;
import view.general.TableToolbar;

public class ConditionTableView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ConditionTable conditionTable = new ConditionTable();
	
	public ConditionTableView() {
		setLayout(new BorderLayout(0,0));
		TableToolbar toolbar = new TableToolbar();
		toolbar.setAddActionListner(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				conditionTable.getModel().addcondition();
				
			}
		});
		
		add(toolbar,BorderLayout.NORTH);
		
		JYTableScrollPane scrollPane = new JYTableScrollPane(conditionTable);
		add(scrollPane,BorderLayout.CENTER);
	}
	
	public ConditionTableModel getConditionTableModel() {
		return conditionTable.getModel();
	}
	
	public ConditionTable getConditionTable() {
		return conditionTable;
	}
}
