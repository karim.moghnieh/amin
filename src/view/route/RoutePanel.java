package view.route;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import de.javasoft.swing.JYTable;
import de.javasoft.swing.JYTableScrollPane;
import de.javasoft.swing.ToolBar;
import view.route.station.ConditionTable;
import view.route.station.ConditionTableModel;
import view.route.station.ConditionTableView;
import view.route.station.StationTableModel;
import view.route.station.StationTableMouseListner;

public class RoutePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StationTableModel stationTableModel;
	private static ConditionTableView conditionTableView;

	/**
	 * Create the panel.
	 */
	public RoutePanel() {

		setLayout(new BorderLayout(0,0));
		
		JToolBar toolBar = new ToolBar(){

			private static final long serialVersionUID = 1L;
			@Override
			public Border getBorder() {
				return new EmptyBorder(0,0,0,0);
			}
			@Override
			public Insets getInsets() {
				return new Insets(0, -3, 0, -3);
			}
		};
		toolBar.setOrientation(SwingConstants.VERTICAL);
		toolBar.setFloatable(false);
		toolBar.setOpaque(false);
		toolBar.setBorder(new EmptyBorder(3, 3, 3, 3));
		
		JButton addButton = new JButton();
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				stationTableModel.addNewStation();
				
			}
		});
		addButton.setToolTipText("Add New Station");
		addButton.setIcon(resources.icons.Icons.ADD);
		addButton.setMaximumSize(new Dimension(30, 30));
		toolBar.add(addButton);
		
		add(toolBar,BorderLayout.WEST);
		
		
		stationTableModel = new StationTableModel();
		JYTable table = new JYTable(stationTableModel);
		table.setFilterRowVisible(true);
		table.addMouseListener(new StationTableMouseListner());
		table.getColumn(3).setCellRenderer(new DeleteTableCellRenderer());
		
		JYTableScrollPane scrollPane = new JYTableScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		
		conditionTableView = new ConditionTableView();
		
		add(conditionTableView, BorderLayout.EAST);
	}
	
	public static ConditionTableView getConditionTableView() {
		return conditionTableView;
	}

}
