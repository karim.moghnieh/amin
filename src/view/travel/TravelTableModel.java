package view.travel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import model.Travel;

public class TravelTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TravelTableModel() {
		alltravels = new ArrayList<>(10);
	}
	
	private List<Travel> alltravels = new ArrayList<>();
	
	private static final Class<?>[] COLUMS_CLASS = {Integer.class, String.class, String.class,Integer.class,String.class,Integer.class, Integer.class,Boolean.class,Date.class};
	private static final String[] COLUMS_NAME = {"Number", "Driver", "Truck","Diesel","Trailer","Distance", "KM","Terminated","Start Date"};
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return COLUMS_CLASS[columnIndex];
	}
	
	@Override
	public String getColumnName(int column) {
		return COLUMS_NAME[column];
	}
	
	@Override
	public int getColumnCount() {
		return COLUMS_NAME.length;
	}
	
	@Override
	public int getRowCount() {
		return alltravels.size();
	}
	
	@Override
	public Object getValueAt(int row, int column) {
		Travel t = new Travel();
		switch (column) {
		case 0:
			return t.getId();
		case 1:
			return t.getDriver().getFirstName();
		case 2:
			return t.getTruck().getName();
		case 3:
			return t.getTruck().getDiesel();
		case 4:
			return t.getTrailer();
		case 5:
			return t.getGlobalDistance();
		case 6:
			return t.isTerminated();
		case 7:
			return t.getStartDate();

		default:
			break;
		}
		return null;
	}
	
	public void setAlltravels(List<Travel> alltravels) {
		this.alltravels = alltravels;
	}
	
	public List<Travel> getAlltravels() {
		return alltravels;
	}
	
	

}
