package view.travel;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import de.javasoft.swing.JYTable;

import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import java.awt.Dimension;

public class TravelPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JSplitPane splitPane;
	private JPanel panel_1;
	private JPanel tablePanel;
	private JScrollPane scrollPane;

	/**
	 * Create the panel.
	 */
	public TravelPanel() {
		setLayout(new BorderLayout(0, 0));
		
		splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		add(splitPane);
		
		panel_1 = new JPanel();
		panel_1.setMinimumSize(new Dimension(10, 100));
		splitPane.setLeftComponent(panel_1);
		
		
		tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout(0,0));
		tablePanel.setMinimumSize(new Dimension(10, 100));
		splitPane.setRightComponent(tablePanel);
		
		scrollPane = new JScrollPane();
		tablePanel.add(scrollPane, BorderLayout.CENTER);
		
		TravelTableModel model = new TravelTableModel();
		JYTable table = new JYTable(model);
		scrollPane.setViewportView(table);


	}

}
