package view.store;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import de.javasoft.swing.JYTable;
import de.javasoft.swing.JYTableHeader;
import de.javasoft.swing.jytable.renderer.CellLayoutHint;
import model.store.Item;
import view.App;
import view.route.DeleteTableCellRenderer;

public class ItemTable extends JYTable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ItemTableModel model = new ItemTableModel();
	
	public ItemTable() {
		super(model);
	    JYTableHeader header =  (JYTableHeader)getTableHeader();
	    CellLayoutHint hint = header.getCellLayoutHint();
	    //center header text
	    header.setCellLayoutHint(new CellLayoutHint(hint.sortMarkerPosition, SwingConstants.CENTER, hint.verticalAlignment));
	    
	    ItemTableMouseListner listner = new ItemTableMouseListner();
	    addMouseListener(listner);
	    
	    getColumn(ItemTableModel.deleteButtonIndex).setCellRenderer(new DeleteTableCellRenderer());
	    
	    setTerminateEditOnFocusLost(true);
	}
	
	@Override
	public ItemTableModel getModel() {
		return model;
	}
	
	private class ItemTableMouseListner extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			if(e.getSource() == null || !(e.getSource() instanceof JTable)){
				return;
			}
			JTable table = (JTable) e.getSource();

			int row = table.rowAtPoint(e.getPoint());
			if(row <0) return;
			int modelRow = table.convertRowIndexToModel(row);
			ItemTableModel tableModel = (ItemTableModel) table.getModel();
			
			if(table.columnAtPoint(e.getPoint()) == ItemTableModel.deleteButtonIndex){
				int answer = JOptionPane.showOptionDialog(App.instatnce, "Delete row", "are you sure ?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] { "Yes",  "No"}, JOptionPane.NO_OPTION);
				if (answer != JOptionPane.YES_OPTION)
					return;
				tableModel.removeItem(modelRow);
			}else{
				Item s = tableModel.getAllItems().get(modelRow);
				RecordTable recordTable = StorePanel.getRecordTableView().getRecordTable();
				if(recordTable.isEditing()){
					recordTable.getCellEditor().cancelCellEditing();
				}
				recordTable.getModel().setItem(s);
			}
		}
	}

	
	

}
