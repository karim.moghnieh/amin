package view.store;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import de.javasoft.swing.JYTableScrollPane;
import view.general.TableToolbar;

public class RecordTableView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RecordTable recordTable = new RecordTable();
	
	public RecordTableView() {
		setLayout(new BorderLayout(0,0));
		TableToolbar toolbar = new TableToolbar();
		toolbar.setAddActionListner(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				recordTable.getModel().addNewRecord();
				
			}
		});
		
		add(toolbar,BorderLayout.NORTH);
		
		JYTableScrollPane scrollPane = new JYTableScrollPane(recordTable);
		add(scrollPane,BorderLayout.CENTER);
	}
	
	public RecordTableModel getRecordTableModel() {
		return recordTable.getModel();
	}
	
	public RecordTable getRecordTable() {
		return recordTable;
	}
}
