package view.store;

import java.awt.BorderLayout;

import javax.swing.JPanel;

public class StorePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ItemTableView itemTableView;
	private static RecordTableView recordTableView;

	/**
	 * Create the panel.
	 */
	public StorePanel() {
		
		setLayout(new BorderLayout(0,0));
		
		itemTableView = new ItemTableView();
		
		add(itemTableView, BorderLayout.CENTER);
		
		recordTableView = new RecordTableView();
		
		add(recordTableView,BorderLayout.SOUTH);
	}
	
	public static ItemTableView getItemTableView() {
		return itemTableView;
	}
	
	public static RecordTableView getRecordTableView() {
		return recordTableView;
	}

}
