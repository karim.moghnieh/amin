package view.store;

import java.util.Date;

import javax.swing.table.AbstractTableModel;

import facade.ItemFacade;
import facade.RecordFacade;
import model.store.Item;
import model.store.Record;

public class RecordTableModel extends AbstractTableModel{
	public static final int deleteButtonIndex = 5;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ItemFacade facade = new ItemFacade();
	private Item item = new Item();
	@Override
	public int getRowCount() {
		return item.getRecords().size();
	}
	
	private static final Class<?>[] COLUMS_CLASS = {String.class, String.class, Date.class,Integer.class, String.class,String.class};
	private static final String[] COLUMS_NAME = 	{"Executor ", "Truck" , "Date",  "Quantity", "Description" ,  "Delete"};
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return COLUMS_CLASS[columnIndex];
	}
	
	@Override
	public String getColumnName(int column) {
		return COLUMS_NAME[column];
	}
	
	@Override
	public int getColumnCount() {
		return COLUMS_NAME.length;
	}


	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if(columnIndex == deleteButtonIndex){
			return "";
		}
		Record r = item.getRecords().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return r.getTo_from();
		case 1:
			return r.getTruck();
		case 2:
			return r.getDate();
		case 3:
			return r.getQuantity();
		case 4:
			return r.getDescription();

		default:
			break;
		}
		return "";
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if(columnIndex == deleteButtonIndex){
			return;
		}
		Record r = item.getRecords().get(rowIndex);
		switch (columnIndex) {
		case 0:
			r.setTo_from(aValue.toString());
			break;
		case 1:
			r.setTruck(aValue.toString());
			break;
		case 2:
			r.setDate((Date)aValue);
			break;
		case 3:
			int q = (Integer) (aValue);
			int oldQuantity = r.getQuantity();
			int dif = q - oldQuantity;
			r.setQuantity(q);
			item.setQuatity(item.getQuatity() + dif);
			int selctedRow = StorePanel.getItemTableView().getItemTable().getSelectedRow();
			ItemTableModel model = StorePanel.getItemTableView().getItemTableModel();
			model.fireTableCellUpdated(selctedRow, ItemTableModel.QuantityColumnIndex);
			
			break;
		case 4:
			r.setDescription(aValue.toString());
			break;
		default:
			break;
		}
		facade.save(item);
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			return true;
			
		}
		return false;
	}
	
	public void setItem(Item item) {
		this.item = item;
		fireTableDataChanged();
	}
	
	public void addNewRecord(){
		Record r = new Record();
		r = new RecordFacade().save(r);
		item.getRecords().add(r);
		int index = item.getRecords().size() -1;
		fireTableRowsInserted(index, index);
		facade.save(item);
	}
	
	public void removeRecord(int index){
		item.getRecords().remove(index);
		fireTableRowsDeleted(index, index);
		facade.save(item);
	}

}
