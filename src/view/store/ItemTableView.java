package view.store;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import de.javasoft.swing.JYTableScrollPane;
import view.general.TableToolbar;

public class ItemTableView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ItemTable itemTable = new ItemTable();
	
	public ItemTableView() {
		setLayout(new BorderLayout(0,0));
		TableToolbar toolbar = new TableToolbar();
		toolbar.setAddActionListner(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				itemTable.getModel().addNewItem();
				
			}
		});
		
		add(toolbar,BorderLayout.NORTH);
		
		JYTableScrollPane scrollPane = new JYTableScrollPane(itemTable);
		add(scrollPane,BorderLayout.CENTER);
	}
	
	public ItemTableModel getItemTableModel() {
		return itemTable.getModel();
	}
	
	public ItemTable getItemTable() {
		return itemTable;
	}
}
