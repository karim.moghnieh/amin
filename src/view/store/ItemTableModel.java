package view.store;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import facade.ItemFacade;
import model.store.Item;

public class ItemTableModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	public static final int deleteButtonIndex = 5;
	public static final int QuantityColumnIndex = 4;
	private ItemFacade facade = new ItemFacade();
	private List<Item> allItems = facade.findAll();

	
	private static final Class<?>[] COLUMS_CLASS = {Integer.class, String.class, String.class,String.class,Integer.class,String.class};
	private static final String[] COLUMS_NAME = {"ID", "Name", "Type", "Model",  "Quantity", "Delete"};
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return COLUMS_CLASS[columnIndex];
	}
	
	@Override
	public String getColumnName(int column) {
		return COLUMS_NAME[column];
	}
	
	@Override
	public int getColumnCount() {
		return COLUMS_NAME.length;
	}
	
	@Override
	public int getRowCount() {
		return allItems.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Item s = allItems.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return s.getId();
		case 1:
			return s.getItemName();
		case 2:
			return s.getItemType();
		case 3:
			return s.getItemModel();
		case 4:
			return s.getQuatity();

		default:
			break;
		}
		return null;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Item s = allItems.get(rowIndex);
		switch (columnIndex) {
		case 1:
			s.setItemName(aValue.toString());
			break;
		case 2:
			s.setItemType(aValue.toString());
			break;
		case 3:
			s.setItemModel(aValue.toString());
			break;
		case 4:
			s.setQuatity(Integer.parseInt(aValue.toString()));
			break;
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(columnIndex == 1 || columnIndex ==2 || columnIndex ==3 || columnIndex ==4){
			return true;
		}
		return false;
	}
	
	public void addNewItem(){
		Item s = new Item();
		s = facade.save(s);
		int size = allItems.size();
		allItems.add(s);
		fireTableRowsInserted(size, size);
	}
	
	public void removeItem(int rwoIndex){
		Item remove = allItems.remove(rwoIndex);
		facade.remove(remove);
		fireTableRowsDeleted(rwoIndex, rwoIndex);
	}
	
	public List<Item> getAllItems() {
		return allItems;
	}
	
	public void setAllItems(List<Item> allItems) {
		this.allItems = allItems;
	}
}
