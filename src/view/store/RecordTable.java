package view.store;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

import de.javasoft.swing.JYTable;
import de.javasoft.swing.JYTableHeader;
import de.javasoft.swing.jytable.renderer.CellLayoutHint;
import de.javasoft.swing.table.DateComboBoxTableCellEditor;
import de.javasoft.swing.table.ObjectTableCellEditor;
import view.App;
import view.route.DeleteTableCellRenderer;

public class RecordTable extends JYTable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static RecordTableModel model = new RecordTableModel();
	
	public RecordTable() {
		super(model);
	    JYTableHeader header =  (JYTableHeader)getTableHeader();
	    CellLayoutHint hint = header.getCellLayoutHint();
	    //center header text
	    header.setCellLayoutHint(new CellLayoutHint(hint.sortMarkerPosition, SwingConstants.CENTER, hint.verticalAlignment));
	    
	    RecordTableMouseListner listner = new RecordTableMouseListner();
	    addMouseListener(listner);
	    
	    getColumn(RecordTableModel.deleteButtonIndex).setCellRenderer(new DeleteTableCellRenderer());
	    
	    setTerminateEditOnFocusLost(true);
	    
	   // getColumn(2).setCellEditor(new DateComboBoxTableCellEditor(getColumn(2).getCellEditor()));
	    installCellEditors(this);
	}
	
	@Override
	public RecordTableModel getModel() {
		return model;
	}
	
	private class RecordTableMouseListner extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			if(e.getSource() == null || !(e.getSource() instanceof JTable)){
				return;
			}
			JTable table = (JTable) e.getSource();

			int row = table.rowAtPoint(e.getPoint());
			if(row <0) return;
			int modelRow = table.convertRowIndexToModel(row);
			RecordTableModel tableModel = (RecordTableModel) table.getModel();
			
			if(table.columnAtPoint(e.getPoint()) == RecordTableModel.deleteButtonIndex){
				int answer = JOptionPane.showOptionDialog(App.instatnce, "Delete row", "are you sure ?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] { "Yes",  "No"}, JOptionPane.NO_OPTION);
				if (answer != JOptionPane.YES_OPTION)
					return;
				tableModel.removeRecord(modelRow);
			}
		}
	}
	
	  private void installCellEditors(JTable table)
	  {
	    TableCellEditor defaultEditor = table.getDefaultEditor(Date.class);
	    DateComboBoxTableCellEditor editor = new DateComboBoxTableCellEditor(defaultEditor);    
	    table.setDefaultEditor(Date.class, editor);
	    
	    //register additional editors to object editor
//	    editor.addEditor(String.class, new TextFieldTableCellEditor(defaultEditor)); 
//	    editor.addEditor(Boolean.class, new CheckBoxTableCellEditor(defaultEditor));
//	    editor.addEditor(Integer.class, new NumberTableCellEditor(defaultEditor, Integer.class, 0, 0));
//	    editor.addEditor(Dimension.class, new IntegerGroupTableCellEditor(defaultEditor, Dimension.class, 2));
//	    editor.addEditor(URL.class, new UneditableTableCellEditor(defaultEditor));
//	    editor.addEditor(TableSeparator.class, new UneditableTableCellEditor(defaultEditor));    
//	    editor.addEditor(SpinnerNumberModel.class, new SpinnerTableCellEditor(defaultEditor));    
//	    editor.addEditor(DefaultBoundedRangeModel.class, new SliderTableCellEditor(defaultEditor));    
	    
	//    boolean editableField = true;
	    //editor.addEditor(ComboBoxModel.class, new ComboBoxTableCellEditor(defaultEditor, editableField));
	   // editor.addEditor(DefaultComboBoxModel.class, new JYComboBoxTableCellEditor(defaultEditor, editableField, false));       
	 //   editor.addEditor(Date.class, new DateComboBoxTableCellEditor(defaultEditor, editableField, false));
	  //  editor.addEditor(Color.class, new ColorComboBoxTableCellEditor(defaultEditor, editableField, false, false));
	 //   editor.addEditor(Font.class, new FontComboBoxTableCellEditor(defaultEditor, editableField, false));

	    //enable edit on doubleclick
	    //editor.setClicksToEdit(2);
	  } 

	
	

}
