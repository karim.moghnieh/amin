package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import de.javasoft.swing.ToolBar;

public class AminToolbar extends ToolBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public AminToolbar() {
		JButton trajectoryButton = new JButton("Trajectory");
		add(trajectoryButton);
		JButton trailerButton = new JButton("Trailler");
		add(trailerButton);
		JButton driverButton = new JButton("Driver");
		add(driverButton);
		JButton truckButton = new JButton("Truck");
		add(truckButton);
		JButton routeButton = new JButton("Route");
		routeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				App.instatnce.showView(App.ROUTE);
				
			}
		});
		add(routeButton);
		
		
		JButton storeButton = new JButton("Store");
		storeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				App.instatnce.showView(App.STORE);
				
			}
		});
		add(storeButton);
	}
}
