package model.trajectory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: TrajectoryPoint
 *
 */
@Entity

public class TrajectoryPoint implements Serializable {
	private static final long serialVersionUID = 1L;

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private String description;
	private int duration;
	private int distanceToNext;
	private int pointOrder;
	
	private List<Condition> conditions = new ArrayList<>();

	public TrajectoryPoint() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getDistanceToNext() {
		return distanceToNext;
	}
	public void setDistanceToNext(int distanceToNext) {
		this.distanceToNext = distanceToNext;
	}
	public int getPointOrder() {
		return pointOrder;
	}
	public void setPointOrder(int pointOrder) {
		this.pointOrder = pointOrder;
	}
	public List<Condition> getConditions() {
		return conditions;
	}
	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}
	
	
   
}
