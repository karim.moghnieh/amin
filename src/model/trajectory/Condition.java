package model.trajectory;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Condition
 *
 */
@Entity

public class Condition implements Serializable {
	private static final long serialVersionUID = 1L;

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private boolean valid;
	private String notice;
	private String driverNotice;

	public Condition() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public String getDriverNotice() {
		return driverNotice;
	}
	public void setDriverNotice(String driverNotice) {
		this.driverNotice = driverNotice;
	}
	
	
   
}
