package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Voucher
 *
 */
@Entity

public class Voucher implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	private String type;
	private Date validityDate;
	private int alertMargin;
	private boolean alert = true;

	public Voucher() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}

	public int getAlertMargin() {
		return alertMargin;
	}

	public void setAlertMargin(int alertMargin) {
		this.alertMargin = alertMargin;
	}
	
	public boolean isAlert() {
		return alert;
	}
	
	public void setAlert(boolean alert) {
		this.alert = alert;
	}
	
	
   
}
