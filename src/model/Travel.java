package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import model.trajectory.TrajectoryPoint;

/**
 * Entity implementation class for Entity: Travel
 *
 */
@Entity

public class Travel implements Serializable {
	private static final long serialVersionUID = 1L;
	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private List<TrajectoryPoint> trajectoryPoints = new ArrayList<>();
	private Driver driver;
	private Truck truck;
	private Trailer trailer;
	private int globalDistance;
	private boolean terminated;
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	public Travel() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public List<TrajectoryPoint> getTrajectoryPoints() {
		return trajectoryPoints;
	}
	public void setTrajectoryPoints(List<TrajectoryPoint> trajectoryPoints) {
		this.trajectoryPoints = trajectoryPoints;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public Truck getTruck() {
		return truck;
	}
	public void setTruck(Truck truck) {
		this.truck = truck;
	}
	public Trailer getTrailer() {
		return trailer;
	}
	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}
	
	public int getGlobalDistance() {
		return globalDistance;
	}
	public void setGlobalDistance(int globalDistance) {
		this.globalDistance = globalDistance;
	}
	
	public boolean isTerminated() {
		return terminated;
	}
	
	public void setTerminated(boolean terminated) {
		this.terminated = terminated;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	
   
}
