package model.route;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: StationPoint
 *
 */
@Entity

public class StationPoint implements Serializable {
	private static final long serialVersionUID = 1L;

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@ManyToOne
	private Station station;
	private int duration;
	private int distanceToNext;
	private int pointOrder;

	public StationPoint() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getDistanceToNext() {
		return distanceToNext;
	}

	public void setDistanceToNext(int distanceToNext) {
		this.distanceToNext = distanceToNext;
	}

	public int getPointOrder() {
		return pointOrder;
	}

	public void setPointOrder(int pointOrder) {
		this.pointOrder = pointOrder;
	}
	
	
	
   
}
