package model.route;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: StaionRoute
 *
 */
@Entity

public class StaionRoute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private String description;
	private int globalDuration;
	@OneToMany
	private List<StationPoint> stationPoints;
	

	public StaionRoute() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getGlobalDuration() {
		return globalDuration;
	}
	public void setGlobalDuration(int globalDuration) {
		this.globalDuration = globalDuration;
	}
	public List<StationPoint> getStationPoints() {
		return stationPoints;
	}
	public void setStationPoints(List<StationPoint> stationPoints) {
		this.stationPoints = stationPoints;
	}
	
	
   
}
