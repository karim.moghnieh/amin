package model.store;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Records
 *
 */
@Entity

public class Record implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String To_from;
	private String truck;
	private String type;
	private int quantity;
	@Temporal(TemporalType.DATE)
	private Date date  = new Date();
	private String description;
	
	
	public Record() {
		super();
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getTruck() {
		return truck;
	}


	public void setTruck(String truck) {
		this.truck = truck;
	}


	public String getTo_from() {
		return To_from;
	}


	public void setTo_from(String to_from) {
		To_from = to_from;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
	
   
}
