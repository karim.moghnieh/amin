package model.store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import org.eclipse.persistence.annotations.PrivateOwned;
import static javax.persistence.CascadeType.ALL;

/**
 * Entity implementation class for Entity: Store
 *
 */
@Entity

public class Item implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@OneToMany(orphanRemoval = true, cascade = ALL)
	@PrivateOwned
	@JoinColumn
	private List<Record> records = new ArrayList<>();
	private String itemName;
	private String itemType;
	private String itemModel;
	private int quatity;

	
	private static final long serialVersionUID = 1L;

	public Item() {
		super();
	}

	public long getId() {
		return id;
	}
	
	public List<Record> getRecords() {
		return records;
	}
	
	public void setRecords(List<Record> records) {
		this.records = records;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemModel() {
		return itemModel;
	}

	public void setItemModel(String itemModel) {
		this.itemModel = itemModel;
	}

	public int getQuatity() {
		return quatity;
	}

	public void setQuatity(int quatity) {
		this.quatity = quatity;
	}
	
	
   
}
