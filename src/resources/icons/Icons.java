package resources.icons;

import javax.swing.ImageIcon;



public class Icons {

	public static final ImageIcon PANORAMA_MENU = new ImageIcon(Icons.class.getResource("/resources/icons/mokhtar.png"));
	
	public static final ImageIcon ADD = new ImageIcon(Icons.class.getResource("/resources/icons/add.png"));
	
	public static final ImageIcon DELETE = new ImageIcon(Icons.class.getResource("/resources/icons/delete.png"));
	
	public static final ImageIcon VIEW = new ImageIcon(Icons.class.getResource("/resources/icons/show.png"));
		
	public static final ImageIcon FOLDER = new ImageIcon(Icons.class.getResource("/resources/icons/folder-gisdata.png"));
	
	public static final ImageIcon SET_POS = new ImageIcon(Icons.class.getResource("/resources/icons/axis3D.png"));
	
	public static final ImageIcon SETTINGS = new ImageIcon(Icons.class.getResource("/resources/icons/preferences.png"));
	
	public static final ImageIcon STYLE = new ImageIcon(Icons.class.getResource("/resources/icons/style.png"));
	
	public static final ImageIcon DEFAULT_STYLE = new ImageIcon(Icons.class.getResource("/resources/icons/markers.png"));
	
	public static final ImageIcon ELEVATION	 = new ImageIcon(Icons.class.getResource("/resources/icons/layer-elevation.png"));
	
	public static final ImageIcon INFO	 = new ImageIcon(Icons.class.getResource("/resources/icons/info.png"));
	
	public static final ImageIcon CAMERA_VIEW	 = new ImageIcon(Icons.class.getResource("/resources/icons/cone.png"));
	
	public static final ImageIcon STOP	 = new ImageIcon(Icons.class.getResource("/resources/icons/stop.png"));
	
	public static final ImageIcon START	 = new ImageIcon(Icons.class.getResource("/resources/icons/start.png"));
	
	public static final ImageIcon PAUSE	 = new ImageIcon(Icons.class.getResource("/resources/icons/pause.png"));
	
	public static final ImageIcon SAVE	 = new ImageIcon(Icons.class.getResource("/resources/icons/Save.png"));
	
	public static final ImageIcon WIZARD	 = new ImageIcon(Icons.class.getResource("/resources/icons/wizard.png"));
	
	public static final ImageIcon RESET	 = new ImageIcon(Icons.class.getResource("/resources/icons/config24.png"));
	
	public static final ImageIcon CONFIG	 = new ImageIcon(Icons.class.getResource("/resources/icons/config16.png"));
	
	public static final ImageIcon GRID	 = new ImageIcon(Icons.class.getResource("/resources/icons/grid.png"));
	
	public static final ImageIcon EXPORT	 = new ImageIcon(Icons.class.getResource("/resources/icons/export.png"));
	
	public static final ImageIcon IMPORT	 = new ImageIcon(Icons.class.getResource("/resources/icons/import.png"));
	
	public static final ImageIcon SHOW_ON_MAP	 = new ImageIcon(Icons.class.getResource("/resources/icons/showOnPanorama.png"));
	
	public static final ImageIcon GO_TO	 = new ImageIcon(Icons.class.getResource("/resources/icons/goto.png"));
}
