package facade;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.internal.jpa.EJBQueryImpl;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.DatabaseQuery;
import org.eclipse.persistence.sessions.Session;



public class AbstractFacade<T> {
	public static final String unitName = "amin";

	public enum Order {
		ASC, DESC;
	}

	protected Class<T> entityClass;

	//@PersistenceContext(unitName = AbstractFacade.unitName)
	protected EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public AbstractFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("amin");
		em = emf.createEntityManager();
	}

	protected EntityManager getEntityManager() {
		return em;
	}

	public void flush() {
		try {
			em.getTransaction().begin();
			em.flush();
			em.getTransaction().commit();
		}
		catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		}
	}

	public T save(T entity) {
		try {
			em.getTransaction().begin();		
			T t = getEntityManager().merge(entity);		
			em.getTransaction().commit();
			return t;
		} 
		catch (Exception e) {
			if(em.getTransaction().isActive())
				em.getTransaction().rollback();
			e.printStackTrace();			
			return entity;
		}
	}

	public void remove(T entity) {
		try
		{
			if(em.getTransaction().isActive())
			{
				getEntityManager().remove(entity);
				em.getTransaction().commit();
			}
			else
			{
				em.getTransaction().begin();
				getEntityManager().remove(entity);
				em.getTransaction().commit();
			}
						
		}		
		catch (Exception e) {
			try
			{
				em.getTransaction().begin();
				getEntityManager().remove(getEntityManager().merge(entity));
				em.getTransaction().commit();
			}
			catch(Exception exe )
			{
				em.getTransaction().rollback();
				e.printStackTrace();
			}
			
		}
		

	}

	public void evict(Object id) throws Exception {
		em.getTransaction().begin();
		getEntityManager().getEntityManagerFactory().getCache()
		.evict(this.entityClass, id);
		em.getTransaction().commit();
	}
	
	public void clearCache() {
		getEntityManager().getEntityManagerFactory().getCache().evictAll();
	}

	public void remove(T[] entityArray) {
		em.getTransaction().begin();
		for (T entity : entityArray) {
			remove(entity);
		}
		em.getTransaction().commit();
	}

	public T find(Object id) {
		T t;
		if(em.getTransaction().isActive())
		{
			t = getEntityManager().find(entityClass, id);
			em.getTransaction().commit();
		}
		else
		{
			em.getTransaction().begin();
			t = getEntityManager().find(entityClass, id);
			em.getTransaction().commit();
		}		
		return t;
	}

	public Object refresh(Object obj)  {
		try
		{
			getEntityManager().refresh(obj);			
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		return obj;
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findAll(){
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<T> r = cq.from(entityClass);
		cq.select(r);
		return getEntityManager().createQuery(cq).getResultList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findAll(String field, Order order) throws Exception {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<T> r = cq.from(entityClass);
		cq.select(r);
		switch (order) {
		case ASC:
			cq.orderBy(cb.asc(r.get(field)));
		case DESC:
			cq.orderBy(cb.desc(r.get(field)));
		}
		return getEntityManager().createQuery(cq).getResultList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findRange(int start, int end) throws Exception {
		CriteriaQuery cq = getEntityManager().getCriteriaBuilder()
				.createQuery();
		cq.select(cq.from(entityClass));
		javax.persistence.Query q = getEntityManager().createQuery(cq);
		q.setMaxResults(start);
		q.setFirstResult(end);
		return q.getResultList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int count() throws Exception {
		CriteriaQuery criteriaQuery = getEntityManager().getCriteriaBuilder()
				.createQuery();
		javax.persistence.criteria.Root<T> root = criteriaQuery
				.from(entityClass);
		criteriaQuery.select(getEntityManager().getCriteriaBuilder()
				.count(root));
		javax.persistence.Query q = getEntityManager().createQuery(
				criteriaQuery);
		return ((Long) q.getSingleResult()).intValue();
	}

	protected void getSqlFromJpql(Query query) {
		@SuppressWarnings("unused")
		Session s = em.unwrap(JpaEntityManager.class).getActiveSession();
		DatabaseQuery dq = ((EJBQueryImpl<?>) query).getDatabaseQuery();
		System.out.println(dq.getSQLString());
	}
	
	
}
