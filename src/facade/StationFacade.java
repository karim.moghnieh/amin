package facade;

import model.route.Station;

public class StationFacade extends AbstractFacade<Station>{

	public StationFacade() {
		super(Station.class);
	}
}
