package facade;

import model.store.Item;

public class ItemFacade extends AbstractFacade<Item>{

	public ItemFacade() {
		super(Item.class);
	}

}
