package facade;

import model.store.Record;

public class RecordFacade extends AbstractFacade<Record>{

	public RecordFacade() {
		super(Record.class);
	}

}
